//
//  RSBViewController.m
//  RSBUndoManager
//
//  Created by Anton Kormakov on 08/04/2016.
//  Copyright (c) 2016 Anton Kormakov. All rights reserved.
//

#import "RSBViewController.h"
#import <RSBUndoManager/RSBUndoManager.h>

@interface RSBViewController () <UITextFieldDelegate>

@property (nonatomic, strong) RSBUndoManager *undoManager;

@property (nonatomic, strong) UITextField *textField1;
@property (nonatomic, strong) UITextField *textField2;

@property (nonatomic, strong) UIButton *undoButton;
@property (nonatomic, strong) UIButton *redoButton;

@property (nonatomic, strong) UILabel *skippingModeLabel;
@property (nonatomic, strong) UISwitch *skippingModeSwitch;

@end

@implementation RSBViewController

@synthesize undoManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.undoManager = [[RSBUndoManager alloc] init];
    self.undoManager.memoryWarningPolicy = RSBUndoManagerMemoryWarningPolicyDropAll;
    self.undoManager.levelsOfUndo = 50;
	
    self.textField1 = [[UITextField alloc] init];
    self.textField1.borderStyle = UITextBorderStyleLine;
    self.textField1.delegate = self;
    [self.view addSubview:self.textField1];
    
    self.textField2 = [[UITextField alloc] init];
    self.textField2.borderStyle = UITextBorderStyleLine;
    self.textField2.delegate = self;
    [self.view addSubview:self.textField2];
    
    self.undoButton = [[UIButton alloc] init];
    [self.undoButton setTitle:@"UNDO" forState:UIControlStateNormal];
    [self.undoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.undoButton addTarget:self action:@selector(onUndoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.undoButton];
    
    self.redoButton = [[UIButton alloc] init];
    [self.redoButton setTitle:@"REDO" forState:UIControlStateNormal];
    [self.redoButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.redoButton addTarget:self action:@selector(onRedoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.redoButton];
    
    self.skippingModeLabel = [[UILabel alloc] init];
    self.skippingModeLabel.text = @"Merge operations";
    [self.view addSubview:self.skippingModeLabel];
    
    self.skippingModeSwitch = [[UISwitch alloc] init];
    [self.view addSubview:self.skippingModeSwitch];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.textField1 becomeFirstResponder];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.textField1.frame = (CGRect){
        .origin.x = 16.0,
        .origin.y = 32.0,
        .size.width = self.view.bounds.size.width - 32.0,
        .size.height = 44.0,
    };
    
    self.textField2.frame = (CGRect){
        .origin.x = 16.0,
        .origin.y = CGRectGetMaxY(self.textField1.frame) + 4.0,
        .size.width = self.view.bounds.size.width - 32.0,
        .size.height = 44.0,
    };
    
    [self.undoButton sizeToFit];
    self.undoButton.center = (CGPoint){
        .x = self.view.bounds.size.width / 4.0,
        .y = CGRectGetMaxY(self.textField2.frame) + 32.0,
    };
    
    [self.redoButton sizeToFit];
    self.redoButton.center = (CGPoint){
        .x = 3.0 * self.view.bounds.size.width / 4.0,
        .y = CGRectGetMaxY(self.textField2.frame) + 32.0,
    };
    
    [self.skippingModeLabel sizeToFit];
    self.skippingModeLabel.center = (CGPoint){
        .x = self.view.bounds.size.width / 2.0,
        .y = CGRectGetMaxY(self.undoButton.frame) + 32.0,
    };
    
    [self.skippingModeSwitch sizeToFit];
    self.skippingModeSwitch.center = (CGPoint){
        .x = self.view.bounds.size.width / 2.0,
        .y = CGRectGetMaxY(self.skippingModeLabel.frame) + 32.0,
    };
}

#pragma mark - Actions

- (void)onUndoButtonPressed:(UIButton *)sender {
    [self.undoManager undo];
}

- (void)onRedoButtonPressed:(UIButton *)sender {
    [self.undoManager redo];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [[self.undoManager prepareWithInvocationTarget:textField] resignFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [[self.undoManager prepareWithInvocationTarget:textField] becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (self.skippingModeSwitch.isOn) {
        [self.undoManager mergeTarget:textField keyPath:@"text"];
    } else {
        [self.undoManager appendTarget:textField keyPath:@"text"];
    }
    
    return YES;
}

@end
