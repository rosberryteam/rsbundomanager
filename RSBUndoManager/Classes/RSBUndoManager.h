//
//  RSBUndoManager.h
//  Pods
//
//  Created by Anton K on 8/4/16.
//
//

#import <Foundation/Foundation.h>

/// Memory warning hanlding policy
typedef enum {
    /// Do nothing
    RSBUndoManagerMemoryWarningPolicyNone,
    /// Drop the oldest half of undo history
    RSBUndoManagerMemoryWarningPolicyDropHalf,
    /// Drop all undo history
    RSBUndoManagerMemoryWarningPolicyDropAll,
} RSBUndoManagerMemoryWarningPolicy;

/*!
 * @discussion Lightweight extension of NSUndoManager w/ keypath-based undo/redo registration & memory awareness.
 */
@interface RSBUndoManager : NSUndoManager


/*!
 * @brief Memory warning handling policy. Default is @c RSBUndoManagerMemoryWarningPolicyNone. @see RSBUndoManagerMemoryWarningPolicy.
 */
@property (nonatomic, assign) RSBUndoManagerMemoryWarningPolicy memoryWarningPolicy;

/*!
 * @brief Register a new undo action. Redo action will be automatically registered when undo is performed.
 * @param target
 * @param keyPath
 */
- (void)appendTarget:(id)target keyPath:(NSString *)keypath;

/*!
 * @brief If last registered undo action has the same target & keyPath, do nothing, otherwise, register a new one. Redo action will be automatically registered when undo is performed.
 * @param target
 * @param keyPath
 */
- (void)mergeTarget:(id)target keyPath:(NSString *)keypath;

#ifdef DEBUG
- (void)printUndoStack;
- (void)printRedoStack;
#endif

@end
