//
//  RSBUndoProxy.h
//  Pods
//
//  Created by Anton K on 8/4/16.
//
//

#import <Foundation/Foundation.h>

@class RSBUndoManager;

@interface RSBUndoProxy : NSProxy

+ (RSBUndoProxy *)proxyWithInstance:(id)object;
- (id)initWithInstance:(id)object;

@property (nonatomic, weak) RSBUndoManager *manager;

@end
